self: super:
let sources = import ../nix/sources.nix;
in {
  vimPlugins = super.vimPlugins // {
    coc-nvim = super.vimUtils.buildVimPluginFrom2Nix {
      pname = "coc-nvim";
      version = "ad49565b";
      src = sources."coc.nvim";
      meta.homepage = "https://github.com/gcoakes/coc.nvim/";
    };
  };
}
